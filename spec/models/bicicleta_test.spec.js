var Bicicleta  = require('../../models/bicicleta');

/*se enfoca en el comportamiento en este caso allBicis
 comenzar la lista vacia
 */
describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    
    });

}); 


describe('Bicicleta.add', () => {
    it('agregamos un registro', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let bici = new Bicicleta(1,'rojo',  'urbana', [-34.6012424, -53.861497]);
        Bicicleta.add(bici);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(bici);
    
    });

}); 



describe('Bicicleta.findById', () => {
    it('DEBERA DEVOLVER ID 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let bici1 = new Bicicleta(1,'rojo',  'urbana', [-34.6012424, -53.861497]);
        let bici2 = new Bicicleta(2,'azul',  'montaña', [-34.6012424, -53.861497]);
        
        Bicicleta.add(bici1);
        Bicicleta.add(bici2);

        var targetBici = Bicicleta.findById(1);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(bici);
    
    });

}); 